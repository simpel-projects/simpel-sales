from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.contenttypes.admin import GenericStackedInline

from simpel_contacts.admin import LinkedAddressInline
from simpel_contacts.models import LinkedContact

admin.site.unregister(get_user_model())


class LinkedContactInline(GenericStackedInline):
    model = LinkedContact
    ct_field = "linked_object_type"
    ct_fk_field = "linked_object_id"
    extra = 0


class NewUserAdmin(UserAdmin):
    inlines = [LinkedContactInline, LinkedAddressInline]


admin.site.register(get_user_model(), NewUserAdmin)
